<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array (
  'site_name' => 'StartBBS- 起点开源论坛-烧饼bbs',
  'index_page' => '',
  'show_captcha' => 'on',
  'show_editor' => 'on',
  'site_close' => 'on',
  'site_close_msg' => '网站升级中，暂时关闭。',
  'basic_folder' => '',
  'version' => 'V1.1.2',
  'static' => 'white',
  'themes' => 'default',
  'auto_tag' => 'on',
  'encryption_key' => 'e10adc3949ba59abbe56e057f20f883e',
);
